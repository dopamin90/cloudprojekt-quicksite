<?php get_header();?>
    
    <div id="content">
        
        <div id="video-bg">
            
        </div><!-- video-bg !--> 
        
        <div id="video-wrapper">
            <div id='playerOqqwxAit'></div>
            <div id="video-replace" style="display: none; position: absolute; top: 0; left: 0; right: 0; bottom: 0;">
                <img src="images/video-replace.png" alt="Video Replace Image" style="max-width: 100%; max-height: 100%;" />    
            </div>
            
            <script type='text/javascript'>
                jwplayer('playerOqqwxAit').setup({
                    playlist: '//jwpsrv.com/feed/OqqwxAit.rss',
                    width: '100%',
                    aspectratio: '16:9',
                    autostart: 'true'
                });
                jwplayer().onComplete( function(event){
                    showVideoOverlay(true);
                });                
            </script>
        </div><!-- wrapper !-->      
            
        <div class="bg-main-content">
        
            <div class="wrapper">
            
                <div class="contrainer-fluid">
                
                    <div class="row">

                        <?php foreach(DataProvider::getContent() as $content):?>
                            <div class="col-lg-4 col-md-4 col-sd-4 col-xs-12 teaser-col">
                                <img src="<?php echo $content->img_path;?>" alt="<?php echo DataProvider::getImgAlt($content->header);?>" class="center" />
                                <h2 class="center">
                                    <?php echo $content->header;?>
                                </h2>
                                <hr class="separator" />
                                <?php foreach($content->text as $contentTxt):?>
                                    <p class="center">
                                        <?php echo $contentTxt;?>
                                    </p>
                                <?php endforeach;?>    
                            </div>
                        <?php endforeach;?>
                    
                    </div>
                
                </div><!-- container fluid !-->
            
            </div><!-- bg main content !-->  
            
        </div><!-- wrapper !--> 
        
        <div class="bg-contact-content">
        
            <div class="wrapper">
            
                <div class="contrainer-fluid">
                
                    <div class="row">
                    
                        <div class="col-lg-6">
                            <?php $footerLeft = DataProvider::getFooter()->left_col; ?>
                            <h3>
                                <?php echo $footerLeft->header;?>
                            </h3>
                            <?php foreach($footerLeft->text as $footerTxt):?>
                                <p><?php echo $footerTxt;?></p>
                            <?php endforeach;?>
                            
                            <h4><?php echo $footerLeft->contact->header;?></h4>
                            <h5 class="subtitle"><?php echo $footerLeft->contact->owner;?></h5>
                            <p>
                                <?php foreach($footerLeft->contact->adress as $footerAdr):?>
                                    <?php echo $footerAdr;?>
                                <?php endforeach;?>
                            </p>
                            <p style="marign-bottom:5px;"><strong><?php echo $footerLeft->contact->phone;?></strong></p>
                            <h4 style="margin-top:0;"><a href="tel:<?php echo DataProvider::getPhone()->phone_links;?>" class="number" style="color:white;"><?php echo DataProvider::getPhone()->number;?></a></h4>
                        </div>
                        
                        <div class="col-lg-6">
                            <form method="post" action="" id="contact-form">
                                <h3>Kontaktformular</h3>
                                <fieldset>

                                    <div id="name-wrapper">
                                        <label for="name" class="control-label">Ihr Name</label>
                                        <input type="text" id="name" name="name" class="form-control" placeholder="z.B.: Max Mustermann" />
                                        <span class="help-block response-helpblock" id="name-helpblock"></span>
                                    </div>

                                    <div id="phone-wrapper">
                                        <label for="phone" class="control-label">Ihre Telefonnummer</label>
                                        <input type="text" id="phone" name="phone" class="form-control" placeholder="z.B.: 0123 - 456 789" />
                                        <span class="help-block response-helpblock" id="phone-helpblock"></span>
                                    </div>

                                    <div id="mail-wrapper">
                                        <label for="mail" class="control-label">Ihre E-Mail-Adresse</label>
                                        <input type="email" id="mail" name="mail" class="form-control" placeholder="z.B.: max@mustermail.de" />
                                        <span class="help-block response-helpblock" id="mail-helpblock"></span>
                                    </div>

                                    <div id="message-wrapper">
                                        <label for="message" class="control-label">Ihre Nachricht</label>
                                        <textarea id="message" name="message" class="form-control" placeholder="Hier Text eingeben ... " style="height:150px;"></textarea>
                                        <span class="help-block response-helpblock" id="message-helpblock"></span>
                                    </div>

                                    <input type="submit" value="Jetzt absenden" id="form-submit-btn"/>                                    
                                </fieldset>
                            </form>
                        </div>
                        
                    </div>
                </div><!-- container fluid !-->
            
            </div><!-- bg main content !-->  
            
        </div><!-- wrapper !--> 
        
        
    </div><!-- content !-->

    <div class="modal fade" id="formSuccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Schließen</span></button>
            <h4 class="modal-title" id="myModalLabel">Rückmeldung</h4>
          </div>
          <div class="modal-body">
            Wie haben Ihre Kontaktanfrage erhalten und werden uns umgehend bei Ihnen melden!
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
          </div>
        </div>
      </div>
    </div>     
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            var $contactForm = $('#contact-form');
            var $submitBtn = $('#form-submit-btn');
            var fields = ['name', 'phone', 'mail', 'message'];

            function addResponseMsg(response, prop) {
                if (response.errors.hasOwnProperty(prop)) {
                    $contactForm.find('#' + prop + "-wrapper").addClass('has-error');
                    $('#' + prop + '-helpblock').text(response.errors[prop]);
                }                                        
            }

            function resetErrors() {
                $contactForm.find('*').removeClass('has-error has-success has-warning');
                $contactForm.find('.response-helpblock').text('');                                        
            }

            $contactForm.submit(function() {

                $submitBtn.prop('disabled', true);

                $.post('form.php', $contactForm.serialize())
                .done(function(response) {
                    $('#formSuccessModal').modal('show');

                    resetErrors();

                    $contactForm.find('fieldset').prop('disabled', true);
                })
                .fail(function(response) {
                    $submitBtn.prop('disabled', false);

                    resetErrors();

                    response = JSON.parse(response.responseText);

                    for (var i = fields.length - 1; i >= 0; i--) {
                        addResponseMsg(response, fields[i]);
                    }
                });
                return false;
            });                                     
        });
    </script>
    <script>
        function showVideoOverlay(on) {
            if (on) {
                $('#video-replace').css('display', 'block');
            } else {
                $('#video-replace').css('display', 'none');
                jwplayer().play();
            }
        }

        $(document).ready(function(){

            $('#video-replace').click(function() {
                showVideoOverlay(false);
            });
           
           function calibrate_everything(){
               
                var vid_h = $('#video-wrapper').height();
                var header_h = $('header').height();
                $('#video-bg').css('height', (vid_h*0.9));
                
                var vid_w = $('#video-wrapper').width();
                var vidMLeft = Math.floor(-(vid_w*0.5));
                $('#video-wrapper').css('marginLeft', vidMLeft);
                
                $('.bg-main-content').css('paddingTop', (vid_h*0.3));
            }
           
           setTimeout(calibrate_everything, 300);
           setInterval(calibrate_everything, 1000);

           $(window).resize(function(){
               calibrate_everything();
           });
              
        });
    </script>
        
<?php get_footer();?>
