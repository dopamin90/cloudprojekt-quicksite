<?php

class DataProvider {

	const DATA_FILE = 'data/data.json';

	private static $data = null;

	private static $phone = null;
	private static $content = null;
	private static $footer = null;

	public static function getPhone() {
		return self::loadData()->phone;
	}

	public static function getContent() {
		return self::loadData()->content;
	}

	public static function getFooter() {
		return self::loadData()->footer;
	}

	public static function getImgAlt($alt) {
		return preg_replace('#<[^>]+>#', ' ', $alt);
	}

	private static function loadData() {
        if (self::$data === null) {
            self::$data = json_decode(file_get_contents(self::DATA_FILE));
        }
        
        return self::$data;
    }
}