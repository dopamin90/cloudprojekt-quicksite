<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <title>Cloud Projekt | &bdquo;Cloudlösungen für den Mittelstand&rdquo;</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/style.css">
    <script src="http://jwpsrv.com/library/Zqf3ZA3PEeSl9SIACyaB8g.js"></script>
    
  </head>
  <body>
      <header>
        
            <div class="wrapper">
            
                <div id="main-logo-box">
                    <a href="index.php"><img src="images/cloud-projekt-logo.jpg" alt="Cloud Projekt Logo" /></a>
                </div>
                
                <div id="call-box">
                    <span class="call-now">Jetzt anrufen</span><br>
                    <a href="tel:<?php echo DataProvider::getPhone()->phone_links;?>" class="number"><?php echo DataProvider::getPhone()->number;?></a>
                </div>
                
            
            </div>
        
        </header>