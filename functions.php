<?php

    require_once('DataProvider.php');

    function get_header($header_name = ''){
        if($header_name != ''){
            require('header-' . $header_name . '.php');
        }else{
            require('header.php');
        }
    }
    
    function get_footer($footer_name = ''){
        if($footer_name != ''){
            require('footer-' . $footer_name . '.php');
        }else{
            require('footer.php');
        }
    }
    
    function load_page($name){
        
        $name = basename($name, '.php');
        $file = 'page-' . $name . '.php';

        if(file_exists($file)){
            require($file);
        }else{
            require('404.php');
        }
        
    }

    function processForm() {
        $result = array('errors'=>array());

        $mandatoryFields = array(
                            'name'=>'Bitte geben Sie hier Ihren Namen an.', 
                            'mail'=>'Bitte geben Sie eine gültige Email an.', 
                            'phone'=>'Bitte geben Sie Ihre Telefonnummer an',
                            'message'=>'Bitte tragen Sie hier Ihre Nachricht ein.');

        foreach ($mandatoryFields as $field => $message) {
            if(!isset($_POST[$field]) || !$_POST[$field]) {
                $result['errors'][$field] = $message;
            }            
        }

        $contactName = trim(filter_var($_POST['name'], FILTER_SANITIZE_STRING));
        $contactMail = filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL);
        $contactPhone = trim(filter_var($_POST['phone'], FILTER_SANITIZE_STRING));        
        $contactMsg = trim(filter_var($_POST['message'], FILTER_SANITIZE_STRING));

        $contactFields = array('name'=>$contactName, 'mail'=>$contactMail, 'phone'=>$contactPhone, 'message'=>$contactMsg);
        foreach ($contactFields as $field => $value) {
            if (!$value) {
                $result['errors'][$field] = $mandatoryFields[$field];
            }
        }

        if(!empty($result['errors'])) {
            return $result;
        }

        $empfaenger = 'bozem@wamd.de'; //info@cloudprojekt.de
        $betreff = 'CloudProjekt - neue Kontaktanfrage';
        $nachricht = 'Es gibt eine neue Kontaktanfrage: ' . "\r\n" .
                "Name: " . $contactName . "\r\n" .
                "Email: " . $contactMail . "\r\n" .
                "Telefon: " . $contactPhone . "\r\n" .
                $contactMsg;

        $header = 'From: kontakt@wamd.de';
        //mail($empfaenger, $betreff, $nachricht, $header);    

        $result['status'] = 'success';
        return $result;    
        
    }        

?>