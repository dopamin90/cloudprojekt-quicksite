    <footer>
        <div class="wrapper">
        
            <div class="links">
                <a id="impressum" class="red" href="/impressum">Impressum</a>&nbsp;&nbsp;&nbsp;
                <a id="datenschutz" class="red" href="/datenschutz">Datenschutz</a>
                
            </div>
            
            <div class="madeby">
                <p>made with <i class="fa fa-heart"></i> by <a class="red" href="http://wamd.de">webakademie</a></p>
            </div>
        </div>
    </footer>
  </body>
</html>